#!/usr/bin/env bash

# base package set
PACKAGES="python-pip python-yaml python-jinja2 python-httplib2 python-paramiko python-pkg-resources"

if [ -f /etc/os-release ] ; then
  . /etc/os-release
  if [ "$ID" = "debian" -o "$ID" = "ubuntu" ] ; then
    PACKAGES+=" python-apt"
  fi
fi

apt install -y --no-install-recommends $PACKAGES
